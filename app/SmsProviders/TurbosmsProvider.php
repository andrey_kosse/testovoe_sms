<?php


namespace App\SmsProviders;


class TurbosmsProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send ()
    {
        $client = new SoapClient ('http://turbosms.in.ua/api/wsdl.html');

        $authResult = $client->Auth(
            [
                'login' => $this->login,
                'password' => $this->pass,
            ]
        );

        if (strpos($authResult->AuthResult, 'успешно') !== false) {
            $balanceResult = $client->GetCreditBalance();

            if (intval($balanceResult->GetCreditBalanceResult) > 0) {
                $smsResult = $client->SendSMS(
                    [
                        'sender' => $this->senderName,
                        'destination' => '+' . trim($this->phone),
                        'text' => $this->text
                    ]
                );

                return [
                    'status'=>$smsResult->SendSMSResult->ResultArray[0],
                    'message'=>$smsResult->SendSMSResult->ResultArray[1]
                ];
            }

            return ['status'=>'error','message'=>'Исчерпан лимит смс'];
        }

        return ['status'=>'error','message'=>$authResult->AuthResult];
    }
}
