<?php


namespace App\SmsProviders;


class AtomProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send(): array
    {
        $src = "<?xml version='1.0' encoding='UTF-8'?>

                <SMS>

                <operations>

                <operation>SEND</operation>

                </operations>

                <authentification>

                <username>{$this->login}</username>

                <password>{$this->pass}</password>

                </authentification>

                <message>

                <sender>{$this->senderName}</sender>

                <text>{$this->text}</text>

                </message>

                <numbers>

                <number>{$this->phone}</number>

                </numbers>

                </SMS>";

        $id = 'empty answer';

        $curl = curl_init();

        $curlOptions = [

            CURLOPT_URL => 'http://atompark.com/members/sms/xml.php',

            CURLOPT_FOLLOWLOCATION => false,

            CURLOPT_POST => true,

            CURLOPT_HEADER => false,

            CURLOPT_RETURNTRANSFER => true,

            CURLOPT_CONNECTTIMEOUT => 15,

            CURLOPT_TIMEOUT => 100,

            CURLOPT_POSTFIELDS => array('XML' => $src),

        ];

        curl_setopt_array($curl, $curlOptions);

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result == false) {
            return ['status' => 'error', 'message' => 'Http request failed!'];
        }

        return ['status' => 'success', 'message' => $result];
    }
}
