<?php


namespace App\SmsProviders;


interface SmsProviderContract
{
    public function send() : array ;
}
