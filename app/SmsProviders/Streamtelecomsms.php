<?php


namespace App\SmsProviders;


class Streamtelecomsms extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $url = "http://gateway.api.sc/get/?user=" . $this->login . "&pwd=" . $this->pass . "&sadr=" . $this->senderName . "&dadr=" . $this->phone . "&text=" . urlencode(
                $this->text
            );

        $answer = $this->get_request($url);

        return $answer ? ['status' => 'success', 'message' => $answer] : ['status' => 'error', 'message' => ''];
    }

    /**
     * @param $url
     * @return bool|string
     */
    private function get_request ($url)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HEADER, false);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $data = curl_exec($curl);

        curl_close($curl);

        return $data;
    }
}
