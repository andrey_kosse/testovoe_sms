<?php


namespace App\SmsProviders;


class Devinotelevibersms extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $data = [

            "resendSms" => true,

            "messages" => [

                [

                    "type" => "viber",

                    "subject" => $this->senderName,

                    "priority" => "high",

                    "validityPeriodSec" => 900,

                    "comment" => "comment",

                    "contentType" => "text",

                    "content" => [

                        "text" => "Здравствуйте! НПФ «ГАЗФОНД пенсионные накопления» признателен за Ваше доверие. Сообщаем, что при переходе в другой Фонд, Вами будет потерян инвестиционный доход. Перейдите по ссылке http://sohrani.gazfond-pn.ru/  и сохраните накопления в полном объеме."

                    ],

                    "address" => $this->phone,

                    "smsText" => $this->text,

                    "smsSrcAddress" => $this->senderNameder,

                    "smsValidityPeriodSec" => 3600

                ]

            ]

        ];

        $res = $this->devino_curl("https://viber.devinotele.com:444/send", $data, $this->login, $this->pass);

        $json = json_decode($res, true);

        return ['status' => $json['status'], 'message' => ''];
    }

    /**
     * @param $url
     * @param $data
     * @param $login
     * @param $pass
     * @return bool|string
     */
    private function devino_curl ($url, $data, $login, $pass)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        curl_setopt($ch, CURLOPT_USERPWD, $login . ":" . $pass);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(

                'Content-Type: application/json',

                'Content-Length: ' . strlen(json_encode($data))
            )

        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);

        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

}
