<?php


namespace App\SmsProviders;


class SmscProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send(): array
    {
        $client = new SoapClient('https://smsc.ru/sys/soap.php?wsdl');

        $data = [
            'login' => $this->login,
            'psw' => $this->pass,
            'phones' => $this->phone,
            'mes' => $this->text,
            'id' => '',
            'time' => 0
        ];

        if (strlen($this->sender_name) > 0) {
            $data["sender"] = $this->sender_name;
        }

        $ret = $client->send_sms($data);

        if ($ret->sendresult->error) {
            return ['status' => 'error', 'message' => 'error № ' . $ret->sendresult->error];
        }

        if (isset($ret->sendresult->id) && intval($ret->sendresult->id) > 0) {
            return ['status' => 'error', 'message' => $ret->sendresult->id];
        }
    }
}
