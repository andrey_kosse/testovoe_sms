<?php


namespace App\SmsProviders;


class SoftlineProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send(): array
    {
        $client = new SoapClient ('WSI.xml');

        $result = $client->sendMessages(
            [
                'alfaName' => $this->senderNamer,

                'contacts' => ['phone' => $this->phone, 'prop' => ''],

                'template' => $this->text,

                'user' => ['password' => $this->pass, 'userName' => $this->login]
            ]
        );

        return ['status' => $result->return->accepted, 'message' => $result->return->notificationID];
    }
}
