<?php


namespace App\SmsProviders;


class SmsProviderFactory
{
    /**
     * @param $gate
     * @return BaseSmsProvider
     */
    public static function create($gate) : BaseSmsProvider
    {
        $type = $gate->type_abbr;

        if(strpos($type,'.') !== false){
            $explode = explode('.',$type);
            $type = array_shift($explode);
        }

        $type = str_replace('_','',$type);

        $providerClassName = 'App\SmsProviders\\'.ucfirst($type)."Provider";

        return new $providerClassName(
            $gate->gate_login,
            $gate->gate_password,
            $gate->signation,
            $gate->sms_text,
            $gate->phone
        );
    }
}
