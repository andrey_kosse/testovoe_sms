<?php


namespace App\SmsProviders;


class MainsmsProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $api = new MainSMS ($this->login, $this->pass, false, false);

        $api->sendSMS($this->phone, $this->text, $this->senderName);

        $response = $api->getResponse();

        return ['status' => $response["status"], 'message' => $response["message"]];
    }
}
