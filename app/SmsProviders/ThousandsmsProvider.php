<?php


namespace App\SmsProviders;


class ThousandsmsProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $res = smsapi_push_msg_nologin($this->login, $this->pass, $this->phone, $this->text);

        if ($res !== null) {
            if (intval($res[0]) == 0) {
                return ['status' => 'success', 'message' => ''];
            }

            return ['status' => 'error', 'message' => $res[0]];
        }

        return ['status' => 'error', 'message' => ''];
    }
}
