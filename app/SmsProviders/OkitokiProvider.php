<?php


namespace App\SmsProviders;


use App\Repositories\Gates\GateRepository;

class OkitokiProvider extends BaseSmsProvider implements SmsProviderContract
{
    /**
     * @var int
     */
    private $gsmId;

    /**
     * @var mixed
     */
    private $repository;

    /**
     * OkitokiProvider constructor.
     * @param string $text
     * @param string $phone
     * @param int $gsmId
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct (string $text, string $phone, int $gsmId)
    {
        $this->text = $text;
        $this->phone = $phone;
        $this->gsmId = $gsmId;

        $this->repository = app()->make(GateRepository::class);
    }

    public function send (): array
    {
        $route = $this->repository->getPostRoute($this->gsmId);

        if (!$route) {
            return false;
        }


        if (intval($route->session_id) > 0 && intval($route->pid) > 1) {
            $query = "select * from ps_pymess('<root><type>lira</type><message>send_sms|" . $route->session_id . "#</message></root>')";
//          $result=pg_query($lvp_conn, $query);

            return ['status'=>'','message'=>''];
        }

        if (intval($route->pid) == 1) {
            if (strlen($this->text) > 70) {
                $tmp = str_split_unicode($this->text, 70);

                foreach ($tmp as $key => $value) {
                    $msg = '<root><type>gsm_command</type><pid>' . str_replace(
                            "<",
                            "",
                            str_replace(">", "", $row['server_pid'])
                        ) . '</pid><gsm_data>{"command_type":"ami","command":"dongle sms ' . $row['post_login'] . ' +' . $phone . ' ' . str_replace(
                            "'",
                            "",
                            $value
                        ) . '","device":"' . $row['post_login'] . '"}</gsm_data></root>';

                    $query = "select * from ps_pymess('" . $msg . "')";

                    $result = pg_query($lvp_conn, $query);

                    sleep(1);
                }
            } else {
                $msg = '<root><type>gsm_command</type><pid>' . str_replace(
                        "<",
                        "",
                        str_replace(">", "", $row['server_pid'])
                    ) . '</pid><gsm_data>{"command_type":"ami","command":"dongle sms ' . $row['post_login'] . ' +' . $phone . ' ' . str_replace(
                        "'",
                        "",
                        $message
                    ) . '","device":"' . $row['post_login'] . '"}</gsm_data></root>';

                $query = "select * from ps_pymess('" . $msg . "')";

                $result = pg_query($lvp_conn, $query);
            }

            return 'true';
        }

        return false;
    }
}
