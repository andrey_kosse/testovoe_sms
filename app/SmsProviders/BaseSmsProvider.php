<?php


namespace App\SmsProviders;


class BaseSmsProvider
{
    protected $login;
    protected $pass;
    protected $senderName;
    protected $text;
    protected $phone;

    /**
     * BaseSmsProvider constructor.
     * @param string $login
     * @param string $pass
     * @param string $senderName
     * @param string $text
     * @param string $phone
     */
    public function __construct(string $login,string $pass,string $senderName,string $text,string $phone)
    {
        $this->login = $login;
        $this->pass = $pass;
        $this->senderName = $senderName;
        $this->text = $text;
        $this->phone = $phone;
    }
}
