<?php


namespace App\SmsProviders;


class MobakProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $smsSender = new Mobak(
            [

                'login' => $this->login,

                'password' => $this->pass,

            ]
        );


        $result = $smsSender->send(
            [

                'message' => $this->text,

                'sender' => $this->senderName,

                'phone' => $this->phone

            ]
        );

        $res = $result->asArray();

        if (isset($res['information']["@attributes"]['code']) && intval(
                $res['information']["@attributes"]['code']
            ) == 0) {
            return ['status' => 'success', 'message' => ''];
        }

        return ['status' => 'error', 'message' => ''];
    }
}
