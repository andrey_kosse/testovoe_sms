<?php


namespace App\SmsProviders;


class FlyProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $xml = '<?xml version="1.0" encoding="utf-8"?>

        <request>

            <operation>SENDSMS</operation>

            <message start_time=" AUTO " end_time=" AUTO " rate="120" lifetime="1" desc="" source="' . $this->senderName . '">

                <body>' . $this->text . '</body>

                <recipient>' . $this->phone . '</recipient>

            </message>

        </request>';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_USERPWD, $this->login . ':' . $this->pass);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_URL, 'http://sms-fly.com/api/api.php');

        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "Accept: text/xml"));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

        $res = curl_exec($ch);

        curl_close($ch);

        return $res ? ['status' => 'success', 'message' => $res] : ['status' => 'error', 'message' => ''];
    }
}
