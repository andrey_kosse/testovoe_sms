<?php


namespace App\SmsProviders;


class DevinotelesmsProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $data = [
            "Login" => $this->login,
            "Password" => $this->pass,
            "DestinationAddress" => $this->phone,
            "SourceAddress" => $this->senderName,
            "Data" => $this->text,
            "Validity" => 0
        ];

        $res = $this->devino_sms_curl("https://integrationapi.net/rest/v2/Sms/Send", $data);

        $json = json_decode($res, true);

        return $json ? ['status' => 'success', 'message' => $json[0]] : ['status' => 'error', 'message' => ''];
    }

    /**
     * @param $url
     * @param $data
     * @return bool|string
     */
    private function devino_sms_curl ($url, $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);

        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}
