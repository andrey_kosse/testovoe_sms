<?php


namespace App\SmsProviders;


class KcellProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $data = [
            "client_message_id" => time(),
            "sender" => $this->senderName,
            "recipient" => $this->phone,
            "message_text" => $this->text,
            "time_bounds" => "ad99"
        ];

        $res = $this->kcell_curl($data, $this->login, $this->pass);

        return $res;
    }

    /**
     * @param $data
     * @param $login
     * @param $pass
     * @return bool|string
     */
    private function kcell_curl ($data, $login, $pass)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.kcell.kz/app/smsgw/rest/v2/messages");

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array('Accept: application/json', 'Content-Type: application/json;charset=utf-8')
        );

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        curl_setopt($ch, CURLOPT_USERPWD, $login . ":" . $pass);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);

        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}
