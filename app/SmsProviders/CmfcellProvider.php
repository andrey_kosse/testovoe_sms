<?php


namespace App\SmsProviders;


class CmfcellProvider extends BaseSmsProvider implements SmsProviderContract
{
    private $res;

    /**
     * @return array
     */
    public function send (): array
    {
        $post_string = '<packet version="1.0">
                            <auth username="' . $this->login . '" password="' . $this->pass . '"/>
                            <command name="SendMessage">
                                <message type="UnicodeSms">
                                    <from>' . $this->senderName . '</from>
                                    <sendDate></sendDate>
                                    <data>' . $this->text . '</data>
                                    <recipients>
                                        <recipient address="+' . $this->phone . '"></recipient>
                                    </recipients>
                                </message>
                            </command>
                            </packet>';

        $fp = fsockopen(
            'smsc.cmfcell.com.ua',
            80,
            $err_num,
            $err_msg,
            30
        ) or die("Socket-openfailed--error: " . $err_num . " " . $err_msg);

        fputs($fp, "POST /sections/service/xmlpost/v1/default.aspx HTTP/1.0\r\n");

        fputs($fp, "Host: oki-toki.ua\r\n");

        fputs($fp, "Content-type: text/xml \r\n");

        fputs($fp, "Content-length: " . strlen($post_string) . " \r\n");

        fputs($fp, "Content-transfer-encoding: text \r\n");

        fputs($fp, "Connection: close\r\n\r\n");

        fputs($fp, $post_string);

        $http_response = '';

        while (!feof($fp)) {
            $http_response .= fgets($fp, 128);
        }

        fclose($fp);

        list($headers, $content) = explode("\r\n\r\n", $http_response, 2);

        $XMLparser = xml_parser_create();

        xml_set_element_handler($XMLparser, [$this, 'cmf_start_element'], [$this, 'cmf_end_element']);

        xml_set_character_data_handler($XMLparser, [$this, 'cmf_str']);

        xml_parse($XMLparser, $content);

        xml_parser_free($XMLparser);

        return ['status' => $this->res['RESULT'], 'message' => $this->res['MESSAGEID']];
    }

    /**
     * @param $parser
     * @param $str
     */
    public function cmf_str ($parser, $str)
    {
        if (strlen(trim($str)) > 0) {
            $this->res[$this->res['current']] = $str;
        }
    }

    /**
     * @param $parser
     * @param $name
     * @param $attrs
     */
    public function cmf_start_element ($parser, $name, $attrs)
    {
        $this->res[$name] = $attrs['TYPE'];
        $this->res['current'] = $name;
    }

    /**
     * @param $parser
     * @param $name
     */
    public function cmf_end_element ($parser, $name)
    {
    }
}
