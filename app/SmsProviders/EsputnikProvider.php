<?php


namespace App\SmsProviders;


class EsputnikProvider extends BaseSmsProvider implements SmsProviderContract
{

    public function send (): array
    {
        $send_sms_url = 'https://esputnik.com/api/v1/message/sms';

        $json_value = new stdClass();

        $json_value->text = $this->text;

        $json_value->from = $this->senderName;

        $json_value->phoneNumbers = [$this->phone];

        $res = $this->send_sp_request($send_sms_url, $json_value, $this->login, $this->pass);

        $json = json_decode($res, true);

        return ['status' => $json['results']['status'], 'message' => ''];
    }

    /**
     * @param $url
     * @param $json_value
     * @param $user
     * @param $password
     * @return bool|string
     */
    private function send_sp_request ($url, $json_value, $user, $password)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_value));

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $password);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}
