<?php

namespace App\Repositories\Gates;


use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Float_;

class GateRepository
{
    /**
     * @param int $gateId
     */
    public function startProcessing (int $gateId)
    {
        DB::table('working_gates')->insert(
            [
                ['gate_id' => $gateId, 'date' => Carbon::now()],
            ]
        );
    }

    /**
     * @param int $gateId
     */
    public function finishProcessing (int $gateId)
    {
        DB::table('working_gates')->where('gate_id', $gateId)->delete();
    }

    /**
     * @param int $gateId
     * @return \Illuminate\Support\Collection
     */
    public function getQueue (int $gateId)
    {
        return DB::table('gate_queue')->where(
            [
                ['sms_text', '<>', null],
                ['gate_id', $gateId],
            ]
        )->take(25)->get();
    }

    /**
     * @param int $gateId
     * @return \Illuminate\Database\Eloquent\Model|Builder|object|null
     */
    public function getGate (int $gateId)
    {
        return DB::table('gates')
            ->join(
                DB::raw('(select id as type_id, type_abbr from gate_types) as gate_type'),
                'gates.id',
                '=',
                'gate_type.type_id'
            )
            ->where(
                [
                    ['gate_id', $gateId],
                    ['enabled', 1],
                    ['has_sms', 1],
                ]
            )->first();
    }

    /**
     * @param int $gateId
     * @param string $company
     * @return float
     */
    public function getPrice (int $gateId, string $company): float
    {
        $priceRow = DB::table('gate_link_to_company')
            ->select(['price'])
            ->where(
                [
                    ['gate_id', $gateId],
                    ['company', $company],

                ]
            )->first();

        return $priceRow ? floatval($priceRow->price) : 0;
    }

    /**
     * @param int $gsmId
     * @return \Illuminate\Database\Eloquent\Model|Builder|object|null
     */
    public function getPostRoute(int $gsmId)
    {
        return DB::table('posts_routes')
            ->select(['session_id', 'pid', 'server_pid', 'post_login'])
            ->leftJoin(
                DB::raw('(select post_id as ppid, server_pid from asterisk_dongles) as a_d'),
                'a_d.ppid',
                '=',
                'id'
            )
            ->where('id',$gsmId)
            ->first();
    }

    /**
     * @param $queueItem
     * @param float $pprice
     * @param float $cprice
     * @param int $gateId
     * @param string $message
     * @param string $status
     */
    public function logSms($queueItem,float $pprice,float $cprice,int $gateId,string $message,string $status)
    {
        DB::table('sms_log')->insert(
            [
                [
                    'some' => 0,
                    'some' => 0,
                    'sms_text' => $queueItem->sms_text,
                    'gate_id' => $gateId,
                    'some' => 0,
                    'created' => Carbon::now(),
                    'pprice' => $pprice,
                    'cprice' => $cprice,
                    'phone' => $queueItem->phone,
                    'schema_name' => $queueItem->schema_name,
                    'crm_id' => $queueItem->crm_id,
                    'message' => $message,
                    'user_id' => $queueItem->user_id,
                    'status' => $status,
                ],
            ]
        );
    }

    /**
     * @param int $id
     */
    public function deleteFromQueue(int $id)
    {
        DB::table('gate_queue')->where('id', $id)->delete();
    }

    /**
     * @param $id
     */
    public function queueSetNotified($id)
    {
        DB::table('gate_queue')->where('id', $id)->update(['notified'=>1]);
    }
}
