<?php
namespace App\Services\Gates;

use App\Repositories\Gates\GateRepository;
use App\SmsProviders\OkitokiProvider;
use App\SmsProviders\SmsProviderFactory;

class GateProcessService
{
    /**
     * @var GateRepository
     */
    private $repository;

    /**
     * GateProcessService constructor.
     * @param GateRepository $repository
     */
    public function __construct(GateRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $gateId
     */
    public function processGate(int $gateId)
    {
        $this->repository->startProcessing($gateId);

        $queue = $this->repository->getQueue($gateId);
        $gate = $this->repository->getGate($gateId);
        $provider = SmsProviderFactory::create($gate);

        foreach ($queue as $item){
            $cprice = $this->repository->getPrice($gateId,$item->schema_name);
            $pprice = $gate->price;

            list($status,$message) = $provider->send();

            $this->repository->logSms($item,$pprice,$cprice,$gateId,$message,$status);

            if($provider instanceof OkitokiProvider){
                $this->repository->queueSetNotified($item->id);
            }else{
                $this->repository->deleteFromQueue($item->id);
            }
        }

        $this->repository->finishProcessing($gateId);
    }
}
