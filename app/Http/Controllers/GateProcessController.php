<?php
namespace App\Http\Controllers;

use App\Services\Gates\GateProcessService;

class GateProcessController extends Controller
{
    /**
     * @var GateProcessService
     */
    private $gateService;

    /**
     * GateProcessController constructor.
     * @param GateProcessService $service
     */
    public function __construct(GateProcessService $service)
    {
        $this->gateService = $service;
    }

    /**
     * @param $gateId
     */
    public function process($gateId)
    {
        $this->gateService->processGate(intval($gateId));
    }
}
